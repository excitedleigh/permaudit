#!/usr/bin/env python
import functools
import logging
import sys
from itertools import chain

from django import setup
from django.conf import settings
from django.contrib.auth import decorators

try:
    from stronghold.utils import is_view_func_public as stronghold_is_public
except ImportError:
    stronghold_is_public = None

logger = logging.getLogger(__name__)


def do_monkey_patch():
    """Monkey-patch login_required and permission_required.

    Call this before calling django.setup().
    """
    # Some explanation for what's going on here: A regular decorator is a
    # function that takes a function and returns a function. permission_required
    # is a decorator with arguments, so it's a function that *returns* a
    # decorator. (In other words, it's a function that returns a function that
    # takes a function and returns a function.) We're replacing
    # permission_required with new_perm_req, so that we can replace the
    # decorator it returns with outer_decorator, in which we annotate the view
    # itself so that we can find out what permission we used later on. (In other
    # words, we're replacing the aforementioned function that returns a function
    # that takes a function and returns a function with a function that calls
    # that function and wraps the function it returns with another function that
    # calls *that* function and mutates the function it returns before returning
    # it.) This even works with CBVs that put their permission_required
    # decorator on the dispatch method, because View.as_view() uses
    # update_wrapper from dispatch to the view it returns. (In other words- you
    # know what, never mind.)

    inner_perm_req = decorators.permission_required

    def new_perm_req(perm, *args, **kwargs):
        inner_decorator = inner_perm_req(perm, *args, **kwargs)

        def outer_decorator(func):
            wrapped_func = inner_decorator(func)
            wrapped_func.__dict__.setdefault('_permissions', []).append(perm)
            return wrapped_func
        functools.update_wrapper(outer_decorator, inner_decorator)
        return outer_decorator

    functools.update_wrapper(new_perm_req, decorators.permission_required)
    decorators.permission_required = new_perm_req

    # This one is even more fun, because login_required has *optional* parameters,
    # so in order to allow its use with or without parentheses, they've written it
    # in such a way that it can either *be* a decorator or *return* a
    # decorator.

    inner_login_req = decorators.login_required

    def new_login_req(function=None, *args, **kwargs):
        if function:
            wrapped_func = inner_login_req(function, *args, **kwargs)
            wrapped_func._login_req = True  # noqa
            return wrapped_func

        inner_decorator = inner_login_req(function, *args, **kwargs)

        def outer_decorator(func):
            wrapped_func = inner_decorator(func)
            wrapped_func._login_req = True  # noqa
            return wrapped_func
        functools.update_wrapper(outer_decorator, inner_decorator)
        return outer_decorator

    functools.update_wrapper(new_login_req, decorators.login_required)
    decorators.login_required = new_login_req


def get_view_info(view_func):
    """Get authentication and permission info about a particular view.

    Only call this after django.setup() has been called.
    """
    permissions_required = view_func.__dict__.get('_permissions', [])
    login_required = getattr(view_func, '_login_req', False)

    if not login_required and stronghold_is_public is not None:
        login_required = not stronghold_is_public(view_func)

    return permissions_required, login_required


def ellipsis(stri, length):
    if not isinstance(stri, unicode):
        stri = stri.decode('utf8')
    if len(stri) > length - 1:
        return stri[:length - 1] + u'\u2026'
    return stri


def print_urls(urllist, level=0, namespaces=None):
    # We've got no way of predicting the fully-qualified name of the dashboard
    # we want because of URL namespaces, so we have to just search for it.
    if namespaces is None:
        namespaces = []

    for entry in urllist:
        if getattr(entry, '_callback', None) is not None:
            perm, login = get_view_info(entry._callback)  # noqa
            print u'{:30} {:30} {:40} {:60} {}'.format(
                ellipsis('  ' * level + entry.regex.pattern, 30),
                ellipsis(
                    ':'.join(chain(namespaces, [entry.name])), 30) if entry.name else '',
                ellipsis(entry._callback.__name__, 40),  # noqa
                ellipsis('{}: {}'.format(len(perm), ', '.join(perm))
                         if perm else '--', 60),
                'Login' if login else 'Anonymous')
        else:
            print '  ' * level, entry.regex.pattern
        if hasattr(entry, 'url_patterns'):
            if hasattr(entry, 'namespace') and entry.namespace is not None:
                new_namespaces = namespaces + [entry.namespace]
            else:
                new_namespaces = namespaces
            print_urls(entry.url_patterns, level + 1, new_namespaces)


if __name__ == "__main__":
    do_monkey_patch()
    setup()
    __import__(settings.ROOT_URLCONF)
    patterns = sys.modules[settings.ROOT_URLCONF].urlpatterns
    print_urls(patterns)
