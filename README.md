# Django Permaudit

A utility script that loops through your Django views and shows you which ones
are restricted to authenticated users, or users with certain permissions.

Supports code that uses the `@login_required` and `@permission_required`,
including Class Based Views that apply these decorators to their `dispatch()`
methods.

## Installation

For now, just download `erd.py`. This will be packaged on PyPI soon.

## Usage

```bash
export DJANGO_SETTINGS_MODULE=<path.to.settings>
python permaudit.py
```
